import { ref } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'

import type { Member } from '@/types/Member'
import { useMemberStore } from './MemberStore'

export const useReceipStore = defineStore('counter', () => {
    const authStore = useAuthStore()
    const memberStore = useMemberStore()
    const savedReceipts = ref<Receipt[]>([])
    const receiptItems = ref<ReceiptItem[]>([])
    const paymentType = ref<string>(''); 
    const receiptDialog = ref(false)
    const receipt = ref<Receipt>({
        id: 1,
        createdDate: new Date(),
        totalBefore: 0,
        memberDiscount: 0,
        total: 0,
        receivedAmount: 0,
        change: 0,
        paymentType: '',
        userId: authStore.currantUser.id,
        user: authStore.currantUser,
        memberId: memberStore.currentMember ? memberStore.currentMember.id : 0,
        member: memberStore.currentMember || undefined
    })

  
function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.products?.id === product.id && item.products?.type === product.type);

    if (index >= 0) {
        receiptItems.value[index].unit++;
    } else {
        let price = product.price;

        if (product.type === "เย็น") {
            price += 5.00;
        } else if (product.type === "ปั่น") {
            price += 10.00;
        }

        const newReceipt: ReceiptItem = {
            id: -1,
            name: product.name,
            member: receipt.value.member as Member,
            paymentType:paymentType.value,
            price: price,
            unit: 1,
            productsId: product.id,
            products: product
        };
        
        receiptItems.value.push(newReceipt);
        console.log(receiptItems.value)
    }
    calReceipt();
}

function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
}
function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
}
function dec(item: ReceiptItem) {
    if (item.unit === 1) {
        removeReceiptItem(item)
    } 
    item.unit--
    calReceipt()
    
}  
function calReceipt() {
    let totalBefore = 0
    for(const item of receiptItems.value){
        totalBefore = totalBefore + (item.price * item.unit)
    }
    receipt.value.totalBefore = totalBefore
    if(memberStore.currentMember){
        receipt.value.total = totalBefore * 0.95
        receipt.value.memberDiscount = totalBefore * 0.05
    }else{
        receipt.value.total = totalBefore
    }
    
}
function showReceiptDialog(){
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
}



function hidePaymentTypeDialog() {
    const receivedAmount = receipt.value.receivedAmount || 0;
    const total = receipt.value.total || 0;

    const change = receivedAmount - total;

    if(paymentType.value==='cash'){
      if (change > 0) {
        paymentType.value='cash'
        saveReceiptDataToStore()
        receiptDialog.value = false;
        memberStore.clearSearch()
        clear()
      }else{
        alert("กรุณาใส่จำนวนเงินให้ถูกต้อง");
      }
    }else if(paymentType.value==='qrcode'){
      paymentType.value='qrcode'
      receipt.value.receivedAmount = 0;  
      receipt.value.change = 0;  
      saveReceiptDataToStore()
      receiptDialog.value = false;
      memberStore.clearSearch()
      clear()
    } 
    else{
        alert("กรุณาเลือกช่องทางการชำระเงิน");
    }
    
}

function calculateChange(): number {
    const receivedAmount = receipt.value.receivedAmount || 0;
    const total = receipt.value.total || 0;

    const change = receivedAmount - total;

    return change;
}
function totalChange(): number {
  const receivedAmount = receipt.value.receivedAmount || 0;
  const total = receipt.value.total || 0;

  const change = receivedAmount - total;
  if(change<0){
    return 0;
  }else{return change;}
  
}

  function setPaymentType(type: string) {
      paymentType.value = type;
  }

  let lastUsedId = 0

  if (savedReceipts.value.length > 0) {
    lastUsedId = Math.max(...savedReceipts.value.map((receipt) => receipt.id))
  }

  function updateCurrentMember(member: Member | null) {
    const memberId = member ? member.id : 0
    receipt.value.memberId = memberId || Math.max(lastUsedId, 1)
    receipt.value.member = member || { id: receipt.value.memberId, name: '-', tel: '-' }
  }

  function saveReceiptDataToStore() {
    console.log(receiptItems.value)
    if (receipt.value.change < 0) {
      alert("don't")
    } 
    else {
        calReceipt()
        console.log(receiptItems.value)
        const savedReceipt = { ...receipt.value }
        savedReceipt.receiptItems = receiptItems.value
        console.log(savedReceipt.receiptItems )
        if (
          savedReceipt.totalBefore !== 0 ||
          savedReceipt.memberDiscount !== 0 ||
          savedReceipt.total !== 0
          
        ) {
          
          lastUsedId++
          savedReceipt.id = lastUsedId
          savedReceipt.member = receipt.value.member
          savedReceipt.paymentType = paymentType.value
          savedReceipts.value.push(savedReceipt)
          console.log('Receipt data saved to store:', savedReceipt)
        }

    }

  }
  function loadReceiptHistory(receiptId: number) {

    const selectedReceipt = savedReceipts.value.find((receipt) => receipt.id === receiptId)

    if (selectedReceipt) {
      const listReceipt: Receipt = {
        id: selectedReceipt.id,
        createdDate: selectedReceipt.createdDate,
        totalBefore: selectedReceipt.totalBefore,
        memberDiscount: selectedReceipt.memberDiscount,
        total: selectedReceipt.total,
        receivedAmount: selectedReceipt.receivedAmount,
        change: selectedReceipt.change,
        paymentType: selectedReceipt.paymentType,
        receiptItems: selectedReceipt.receiptItems || [],
        userId: selectedReceipt.userId,
        user: selectedReceipt.user,
        memberId: selectedReceipt.memberId,
        member: selectedReceipt.member ?? undefined
      }

      receipt.value = listReceipt
      receiptDialog.value = true
      console.log(listReceipt)
    } else {
      console.error(`Receipt with id ${receiptId} not found in savedReceipts`)
    }
    
  }
  function clear(){
    receipt.value.id = 0;   
    receipt.value.totalBefore= 0,
    receipt.value.memberDiscount= 0,    
    receipt.value.total= 0,
    receipt.value.receivedAmount= 0,
    receipt.value.change= 0,
    receipt.value.paymentType= '',
    receipt.value.userId= authStore.currantUser.id,
    receipt.value.user= authStore.currantUser,
    receipt.value.memberId= 0
    memberStore.clear()
    receiptItems.value = []
}

  return {
    receiptItems, receipt, receiptDialog,paymentType,savedReceipts,
    addReceiptItem, removeReceiptItem,inc,dec,calReceipt,showReceiptDialog,clear,hidePaymentTypeDialog,calculateChange,
    setPaymentType,updateCurrentMember,saveReceiptDataToStore,loadReceiptHistory,totalChange,}
})
