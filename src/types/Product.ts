type Product = {
    id: number;
    name: string;
    price: number;
    type: string;
}
export{ type Product }